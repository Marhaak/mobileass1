/**
 * http://www.androidhive.info/2014/05/android-working-with-volley-library-1/
 */

package com.example.marius.ass1imt3662;

import android.app.Activity;
import android.os.Bundle;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import java.io.UnsupportedEncodingException;
import android.view.View;
import android.widget.Button;
import com.android.volley.Cache;
import com.android.volley.Cache.Entry;

/**
 * This class takes care of the web activity and will download the image in the IMAGE_URL when asked.
 */
public class Web extends Activity {

    private static final String TAG = Web.class
            .getSimpleName();
    private Button btnImageReq;
    private NetworkImageView imgNetWorkView;

    final static String IMAGE_URL = "http://gtl-gear.appspot.com/static/img/gtl-logo_h57.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        btnImageReq = (Button) findViewById(R.id.getWebButton);
        imgNetWorkView = (NetworkImageView) findViewById(R.id.imgNetwork);


        btnImageReq.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                makeImageRequest();
            }
        });
    }

    private void makeImageRequest() {
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        // If you are using NetworkImageView
        imgNetWorkView.setImageUrl(IMAGE_URL, imageLoader);

        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Entry entry = cache.get(IMAGE_URL);
        if(entry != null){
            try {
                String data = new String(entry.data, "UTF-8");
                // handle data, like converting it to xml, json, bitmap etc.,
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }else{
            // cached response doesn't exists. Make a network call here
        }

    }
}
