/**
 * http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/ some is taken from that url and some is self made
 */

package com.example.marius.ass1imt3662;

import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * This is the class that controls the main activity.
 */
public class MainActivity extends Activity implements
       OnItemSelectedListener {

    Spinner spinner;

    Button buttonAdd;

    EditText inputLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner) findViewById(R.id.spinner);
        buttonAdd = (Button) findViewById(R.id.addToSpinner);
        inputLabel = (EditText) findViewById(R.id.inputTextField);

        spinner.setOnItemSelectedListener(this);

        loadSpinnerData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Invoked by the "Gps" button
    public void startGPS(View view){

        Intent intent = new Intent(this, Gps.class);
        startActivity(intent);
    }

    // Invoked by the "Web" button
    public void startWeb(View view){

        Intent intent = new Intent(this, Web.class);
        startActivity(intent);
    }
    // Invoked by the "Click to add to spinner" button
    public void addName(View view) {

        String label = inputLabel.getText().toString();

        if (label.trim().length() > 0) {
            // database handler
            DatabaseHandler db = new DatabaseHandler(
                    getApplicationContext());

            // inserting new label into database
            db.insertLabel(label);

            // making input filed text to blank
            inputLabel.setText("");

            // Hiding the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(inputLabel.getWindowToken(), 0);

            // loading spinner with newly added data
            loadSpinnerData();
        } else {
            Toast.makeText(getApplicationContext(), "Please enter label name",
                    Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Function to load the spinner data from SQLite database
     * */
    private void loadSpinnerData() {
        // database handler
        DatabaseHandler db = new DatabaseHandler(this);

        // Spinner Drop down elements
        List<String> lables = db.getAllLabels();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String label = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "You selected: " + label,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
